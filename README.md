# Lessons Overview

## Part 1 - Programming in Python

- [Lesson 1 - Running and Quitting](lessons/01-run-quit.rst)
- [Lesson 2 - Variables](lessons/02-variables.rst)
- [Lesson 3 - Types and Conversions](lessons/03-types-conversion.rst)
- [Lesson 4 - Built-in Types](lessons/04-built-in.rst)
- [Coffee Break](lessons/05-coffee.rst)
- [Lesson 5 - Lists](lessons/11-lists.rst)
- [Lesson 6 - For-Loops](lessons/12-for-loops.rst)
- [Lesson 7 - Conditionals](lessons/13-conditionals.rst)
- [Lesson 8 - Libraries](lessons/06-libraries.rst)

## Part 2 - Plotting in Python

- [Lesson 9 - Reading Data](lessons/07-reading-tabular.rst)
- [Lesson 10 - Data Frames](lessons/08-data-frames.rst)
- [Lesson 11 - Plotting](lessons/09-plotting.rst)
- [Lesson 12 - Looping over Datasets](lessons/14-looping-data-sets.rst)
- [Coffee Break](lessons/15-coffee.rst)
- [Lesson 13 - Functions](lessons/16-writing-functions.rst)
- [Lesson 14 - Scope](lessons/17-scope.rst)
- [Lesson 15 - Code Style](lessons/18-style.rst)
- [Lesson 16 - Wrap-Up](lessons/19-wrap.rst)
<!-- - [Lesson 17](lessons/20-feedback.rst) -->

## References

The workshop materials are based on the [_Python Novice Gapminder_ lesson by the Carpentries](https://github.com/swcarpentry/python-novice-gapminder) with some custom modifications.

- [software-carpentry.org](https://software-carpentry.org)
