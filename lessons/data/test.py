import datetime

with open(
    r"C:\Users\Michael Rudolf\HESSENBOX-DA\GitRepos\python-swc-workshop-intern\lessons\data\weather_data.dat",
    "rt",
    encoding="utf-8",
) as dat_file:
    # flag to see if we already reached the data
    real_data = False

    # Precreate a dictionary to be filled with data:
    data = dict()

    for line in dat_file:
        # See if we have normal or Metadata:
        if line.startswith("#"):
            separated = line[1:-1].split(": ")
            key = separated[0]
            value = separated[1]

            # To distinguish the type of data we look for contents in the keys:
            if "Datei" in key or "Von" in key or "Bis" in key:
                value_converted = datetime.datetime.strptime(
                    value, "%d.%m.%Y %H:%M"
                )
            elif "Stationsnummer" in key:  # Positive number
                value_converted = int(value)
            elif "Koordinate" in key:
                value_converted = float(value)
            else:
                value_converted = value

            # After conversion we add the data to the dictionary:
            data[key] = value_converted

        # If not we arrived at the "real" data:
        else:
            # The first time we are still at the headers:
            if not real_data:
                separated_header = line[:-1].split("\t")

                # The headers define the names for the data lists:
                for header in separated_header:
                    data[header] = []  # Preallocate an empty list

                # after we have read this we can set our flag to True
                real_data = True
            else:
                # Now we can read the rest as data:
                separated_data = line[:-1].split("\t")

                # First convert the data
                converted_data = [
                    datetime.datetime.strptime(
                        separated_data[0], "%d.%m.%Y %H:%M"
                    ),
                    float(separated_data[1].replace(",", ".")),
                    separated_data[2],
                ]
                # Then append each datapoint to its list
                for ii, key in enumerate(separated_header):
                    data[key].append(converted_data[ii])

# # Show the contents of data
# for key in data.keys():
#     print(key + ":", data[key])


import matplotlib.pyplot as plt

fig, ax = plt.subplots()
ax.bar(data["Zeitpunkt"], data["Messwert"])
ax.set_xlabel("Zeit")
ax.set_ylabel(f'{data["Parameter"]} ({data["Einheit"]})')
ax.set_title(f'{data["Parameter"]} in {data["Stationsname"]}')
# plt.show()

with open(
    r"C:\Users\Michael Rudolf\HESSENBOX-DA\GitRepos\python-swc-workshop-intern\lessons\data\output.dat",
    "wt",
) as output_file:
    output_file.write("Zeitpunkt, Messwert\n")
    for ii in range(len(data["Zeitpunkt"])):
        output_file.write(f'{data["Zeitpunkt"][ii]},{data["Messwert"][ii]}\n')


import json

from django.core.serializers.json import DjangoJSONEncoder

with open(
    r"C:\Users\Michael Rudolf\HESSENBOX-DA\GitRepos\python-swc-workshop-intern\lessons\data\output.json",
    "wt",
    encoding="utf-8",
) as json_file:
    json.dump(data, json_file, cls=DjangoJSONEncoder)


with open(
    r"C:\Users\Michael Rudolf\HESSENBOX-DA\GitRepos\python-swc-workshop-intern\lessons\data\output.json",
    "rt",
    encoding="utf-8",
) as json_file:
    data_loaded = json.load(json_file)
    print(data_loaded)

import pickle

with open(
    r"C:\Users\Michael Rudolf\HESSENBOX-DA\GitRepos\python-swc-workshop-intern\lessons\data\output.pkl",
    "wb",
) as pkl_file:
    pickle.dump(data, pkl_file)


with open(
    r"C:\Users\Michael Rudolf\HESSENBOX-DA\GitRepos\python-swc-workshop-intern\lessons\data\output.pkl",
    "rb",
) as pkl_file:
    pickle_loaded = pickle.load(pkl_file)
    print(pickle_loaded)
