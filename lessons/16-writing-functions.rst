=============================
Lesson 13 - Writing Functions
=============================

Teaching: 10 min
Exercises: 15 min

-------------------

.. highlights:: **Highlights**

  - Explain and identify the difference between function definition and function call.
  - Write a function that takes a small, fixed number of arguments and produces a single result.

.. hint::

  - How can I create my own functions?

-------------------

What are Functions?
===================

- Break programs down into functions to make them easier to understand.
- Human beings can only keep a few items in working memory at a time.
- Understand larger/more complicated ideas by understanding and combining pieces.

  - Components in a machine.
  - Lemmas when proving theorems.

- Functions serve the same purpose in programs.

  - *Encapsulate* complexity so that we can treat it as a single "thing".

- Also enables *re-use*.

  - Write one time, use many times.

The `def` statement
-------------------

- Define a function using `def` with a name, parameters, and a block of code.
- Begin the definition of a new function with `def`.
- Followed by the name of the function.

  - Must obey the same rules as variable names.

- Then *parameters* in parentheses.

  - Empty parentheses if the function doesn't take any inputs.
  - We will discuss this in detail in a moment.

- Then a colon.
- Then an indented block of code.

.. code-block:: python
  :linenos:

  def print_greeting():
      print('Hello!')
      print('The weather is nice today.')
      print('Right?')


Calling Functions
-----------------

- Defining a function does not run it.

  - Like assigning a value to a variable.

- Must call the function to execute the code it contains.

.. code-block:: python
  :linenos:

  print_greeting()

.. code-block::

  Hello!

Function Arguments
------------------

- Functions are most useful when they can operate on different data.
- Specify *parameters* when defining a function.

  - These become variables when the function is executed.
  - Are assigned the arguments in the call (i.e., the values passed to the function).
  - If you don't name the arguments when using them in the call, the arguments will be matched to parameters in the order the parameters are defined in the function.

.. code-block:: python
  :linenos:

  def print_date(year, month, day):
      joined = str(year) + '/' + str(month) + '/' + str(day)
      print(joined)

  print_date(1871, 3, 19)

.. code-block::

  1871/3/19

Or, we can name the arguments when we call the function, which allows us to specify them in any order and adds clarity to the call site; otherwise as one is reading the code they might forget if the second argument is the month or the day for example.

.. code-block:: python
  :linenos:

  print_date(month=3, day=19, year=1871)

.. code-block::

  1871/3/19

- `()` contains the ingredients for the function
- while the body contains the recipe.

Function Returns
----------------

- Functions may return a result to their caller using `return`.
- Use `return ...` to give a value back to the caller.
- May occur anywhere in the function.
- But functions are easier to understand if `return` occurs:

  - At the start to handle special cases.
  - At the very end, with a final result.

.. code-block:: python
  :linenos:

  def average(values):
      if len(values) == 0:
          return None
      return sum(values) / len(values)

.. code-block:: python
  :linenos:

  a = average([1, 3, 4])
  print('average of actual values:', a)

.. code-block::

  average of actual values: 2.6666666666666665

.. code-block:: python
  :linenos:

  print('average of empty list:', average([]))

.. code-block::

  average of empty list: None

- Remember: **every function returns something.**
- A function that doesn't explicitly `return` a value automatically returns `None`.

.. code-block:: python
  :linenos:

  result = print_date(1871, 3, 19)
  print('result of call is:', result)

.. code-block::

  1871/3/19
  result of call is: None

.. attention:: **Identifying Syntax Errors**

  1. Read the code below and try to identify what the errors are *without* running it.
  2. Run the code and read the error message. Is it a `SyntaxError` or an `IndentationError`?
  3. Fix the error.
  4. Repeat steps 2 and 3 until you have fixed all the errors.

  .. code-block:: python
    :linenos:

    def another_function
      print("Syntax errors are annoying.")
      print("But at least python tells us about them!")
      print("So they are usually not too hard to fix.")

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. code-block:: python
    :linenos:

    def another_function():
      print("Syntax errors are annoying.")
      print("But at least Python tells us about them!")
      print("So they are usually not too hard to fix.")

  .. raw:: html

    </details>

-------------------

.. attention:: **Definition and Use**

  What does the following program print?

  .. code-block:: python
    :linenos:

    def report(pressure):
        print('pressure is', pressure)

    print('calling', report, 22.5)

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. code-block::

    calling <function report at 0x7fd128ff1bf8> 22.5

  A function call always needs parenthesis, otherwise you get memory address of the function object. So, if we wanted to call the function named report, and give it the value 22.5 to report on, we could have our function call as follows

  .. code-block:: python
    :linenos:

    print("calling")
    report(22.5)

  .. code-block::

    calling
    pressure is 22.5

  .. raw:: html

    </details>

-------------------

.. attention:: **Order of Operations**

  1. What's wrong in this example?

  .. code-block:: python
    :linenos:

    result = print_time(11, 37, 59)

    def print_time(hour, minute, second):
      time_string = str(hour) + ':' + str(minute) + ':' + str(second)
      print(time_string)

  2. After fixing the problem above, explain why running this example code:

  .. code-block:: python
    :linenos:

    result = print_time(11, 37, 59)
    print('result of call is:', result)

    gives this output:

  .. code-block::

    11:37:59
    result of call is: None

  3. Why is the result of the call `None`?

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  1. The problem with the example is that the function `print_time()` is defined *after* the call to the function is made. Python doesn't know how to resolve the name `print_time` since it hasn't been defined yet and will raise a `NameError` e.g., `NameError: name 'print_time' is not defined`
  2. The first line of output `11:37:59` is printed by the first line of code, `result = print_time(11, 37, 59)` that binds the value returned by invoking `print_time` to the variable `result`. The second line is from the second print call to print the contents of the `result` variable.
  3. `print_time()` does not explicitly `return` a value, so it automatically returns `None`.

  .. raw:: html

    </details>

-------------------

.. attention:: **Encapsulation**

  Fill in the blanks to create a function that takes a single filename as an argument, loads the data in the file named by the argument,
  and returns the minimum value in that data.

  .. code-block:: python
    :linenos:

    import pandas as pd

    def min_in_data(____):
        data = ____
        return ____

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. code-block:: python
    :linenos:

    import pandas as pd

    def min_in_data(filename):
        data = pd.read_csv(filename)
        return data.min()

  .. raw:: html

    </details>

-------------------

.. attention:: **Find the First**

  Fill in the blanks to create a function that takes a list of numbers as an argument and returns the first negative value in the list.

  What does your function do if the list is empty? What if the list has no negative numbers?

  .. code-block:: python
    :linenos:

    def first_negative(values):
        for v in ____:
            if ____:
                return ____

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. code-block:: python
    :linenos:

    def first_negative(values):
        for v in values:
            if v < 0:
                return v

  If an empty list or a list with all positive values is passed to this function, it returns `None`:

  .. code-block:: python
    :linenos:

    my_list = []
    print(first_negative(my_list))

  .. code-block::

    None

  .. raw:: html

    </details>

-------------------

.. attention:: **Calling by Name**

  Earlier we saw this function:

  .. code-block:: python
    :linenos:

    def print_date(year, month, day):
        joined = str(year) + '/' + str(month) + '/' + str(day)
        print(joined)

  We saw that we can call the function using *named arguments*, like this:

  .. code-block:: python
    :linenos:

    print_date(day=1, month=2, year=2003)

  1. What does `print_date(day=1, month=2, year=2003)` print?
  2. When have you seen a function call like this before?
  3. When and why is it useful to call functions this way?

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  1. `2003/2/1`
  2. We saw examples of using *named arguments* when working with the pandas library. For example, when reading in a dataset using `data = pd.read_csv('data/gapminder_gdp_europe.csv', index_col='country')`, the last argument `index_col` is a named argument.
  3. Using named arguments can make code more readable since one can see from the function call what name the different arguments have inside the function. It can also reduce the chances of passing arguments in the wrong order, since by using named arguments the order doesn't matter.

  .. raw:: html

    </details>

----------------------------

Special Function Arguments
==========================

Keyword Arguments (defaults)
----------------------------

- Sometimes one would want a default value for a function argument

  - For example, occasionally the color of a plot should be red instead of blue
  - Many functions in libraries have additional options called *keyword arguments* (short `kwargs`).

- Assume that we want the `print_date` function to return a base date if no other arguments are given

.. code-block:: python
  :linenos:

  def print_date(year=1970, month=1, day=1):
      joined = str(year) + '/' + str(month) + '/' + str(day)
      print(joined)

  print_date()
  print_date(year=2001)

.. code-block::

  1970/1/1
  2001/1/1

This can also be mixed, but keyword arguments should always be defined after normal arguments

.. code-block:: python
  :linenos:

  def print_date(day, month, year=1970):
      joined = str(year) + '/' + str(month) + '/' + str(day)
      print(joined)

Asterix Idioms
--------------

In many documentations you will see that arguments or keyword arguments are given as `*args` or `**kwargs`. This is a placeholder for an arbitrary number of arguments that can be accepted by the function.

Suppose we want to print all values that are given as input into a function.

.. code-block:: python
  :linenos:

  def print_all(*args):
      for a in args:
          print(a)

  print_all(1)

  print_all(3, 4, 6, 7)

.. code-block::

  1

  3
  4
  6
  7

The same goes for keyword arguments but these are stored in a dictionary.

.. code-block:: python
  :linenos:

  def print_quark(**kwargs):
      for a in kwargs:
          print(a, kwargs[a])

  print_quark(year=1989, weekday='Monday')

.. code-block::

  year 1989
  weekday Monday

Unpacking Function Inputs
-------------------------

Another use of `*` is to unpack iterables to give them to functions

.. code-block:: python
  :linenos:

  def print_date(year, month, day):
      joined = str(year) + '/' + str(month) + '/' + str(day)
      print(joined)

  date_tuple = (2023, 12, 31)
  print_date(*date_tuple)

  date_dict = {"year": 2021, "month": 4, "day": 1}
  print_date(**date_dict)


.. code-block::

  2023/12/31
  2021/4/1

-------------------

.. attention:: **Encapsulation of an If/Print Block**

  The code below will run on a label-printer for chicken eggs.  A digital scale will report a chicken egg mass (in grams) to the computer and then the computer will print a label.

  .. code-block:: python
    :linenos:

    import random
    for i in range(10):

        # simulating the mass of a chicken egg
        # the (random) mass will be 70 +/- 20 grams
        mass = 70 + 20.0 * (2.0 * random.random() - 1.0)

        print(mass)

        # egg sizing machinery prints a label
        if mass >= 85:
            print("jumbo")
        elif mass >= 70:
            print("large")
        elif mass < 70 and mass >= 55:
            print("medium")
        else:
            print("small")

  The if-block that classifies the eggs might be useful in other situations, so to avoid repeating it, we could fold it into a function, `get_egg_label()`. Revising the program to use the function would give us this:

  .. code-block:: python
    :linenos:

    # revised version
    import random
    for i in range(10):

        # simulating the mass of a chicken egg
        # the (random) mass will be 70 +/- 20 grams
        mass = 70 + 20.0 * (2.0 * random.random() - 1.0)

        print(mass, get_egg_label(mass))


  1. Create a function definition for `get_egg_label()` that will work with the revised program above.  Note that the `get_egg_label()` function's return value will be important. Sample output from the above program would be `71.23 large`.
  2. A dirty egg might have a mass of more than 90 grams, and a spoiled or broken egg will probably have a mass that's less than 50 grams.  Modify your `get_egg_label()` function to account for these error conditions. Sample output could be `25 too light, probably spoiled`.

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. code-block:: python
    :linenos:

    def get_egg_label(mass):
        # egg sizing machinery prints a label
        egg_label = "Unlabelled"
        if mass >= 90:
            egg_label = "warning: egg might be dirty"
        elif mass >= 85:
            egg_label = "jumbo"
        elif mass >= 70:
            egg_label = "large"
        elif mass < 70 and mass >= 55:
            egg_label = "medium"
        elif mass < 50:
            egg_label = "too light, probably spoiled"
        else:
            egg_label = "small"
        return egg_label

  .. raw:: html

    </details>

-------------------

.. attention:: **Encapsulating Data Analysis**

  Assume that the following code has been executed:

  .. code-block:: python
    :linenos:

    import pandas as pd

    data_asia = pd.read_csv('data/gapminder_gdp_asia.csv', index_col=0)
    japan = data_asia.loc['Japan']

  1. Complete the statements below to obtain the average GDP for Japan across the years reported for the 1980s.

    .. code-block:: python
      :linenos:

      year = 1983
      gdp_decade = 'gdpPercap_' + str(year // ____)
      avg = (japan.loc[gdp_decade + ___] + japan.loc[gdp_decade + ___]) / 2

  2. Abstract the code above into a single function.

    .. code-block:: python
      :linenos:

      def avg_gdp_in_decade(country, continent, year):
          data_countries = pd.read_csv('data/gapminder_gdp_'+___+'.csv',delimiter=',',index_col=0)
          ____
          ____
          ____
          return avg

  3. How would you generalize this function if you did not know beforehand which specific years occurred as columns in the data? For instance, what if we also had data from years ending in 1 and 9 for each decade?

  .. hint::

    Use the columns to filter out the ones that correspond to the decade, instead of enumerating them in the code.

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  1. The average GDP for Japan across the years reported for the 1980s is computed with:

    .. code-block:: python
      :linenos:

      year = 1983
      gdp_decade = 'gdpPercap_' + str(year // 10)
      avg = (japan.loc[gdp_decade + '2'] + japan.loc[gdp_decade + '7']) / 2

  2. That code as a function is:

    .. code-block:: python
      :linenos:

      def avg_gdp_in_decade(country, continent, year):
          data_countries = pd.read_csv('data/gapminder_gdp_' + continent + '.csv', index_col=0)
          c = data_countries.loc[country]
          gdp_decade = 'gdpPercap_' + str(year // 10)
          avg = (c.loc[gdp_decade + '2'] + c.loc[gdp_decade + '7'])/2
          return avg

  3. To obtain the average for the relevant years, we need to loop over them:

    .. code-block:: python
      :linenos:

      def avg_gdp_in_decade(country, continent, year):
          data_countries = pd.read_csv('data/gapminder_gdp_' + continent + '.csv', index_col=0)
          c = data_countries.loc[country]
          gdp_decade = 'gdpPercap_' + str(year // 10)
          total = 0.0
          num_years = 0
          for yr_header in c.index: # c's index contains reported years
              if yr_header.startswith(gdp_decade):
                  total = total + c.loc[yr_header]
                  num_years = num_years + 1
          return total/num_years

  The function can now be called by:

  .. code-block:: python
    :linenos:

    avg_gdp_in_decade('Japan','asia',1983)

  .. code-block::

    20880.023800000003

  .. raw:: html

    </details>

-------------------

.. attention:: **Simulating a dynamical system**

  In mathematics, a `dynamical system <https://en.wikipedia.org/wiki/Dynamical_system>`__ is a system in which a function describes the time dependence of a point in a geometrical space. A canonical example of a dynamical system is the `logistic map <https://en.wikipedia.org/wiki/Logistic_map>`__, a growth model that computes a new population density (between  0 and 1) based on the current density. In the model, time takes discrete values 0, 1, 2, ...

  1. Define a function called `logistic_map` that takes two inputs: `x`, representing the current population (at time `t`), and a parameter `r = 1`. This function should return a value representing the state of the system (population) at time `t + 1`, using the mapping function:

    `f(t+1) = r * f(t) * [1 - f(t)]`

  2. Using a `for` or `while` loop, iterate the `logistic_map` function defined in part 1, starting from an initial population of 0.5, for a period of time `t_final = 10`. Store the intermediate results in a list so that after the loop terminates you have accumulated a sequence of values representing the state of the logistic map at times `t = [0,1,...,t_final]` (11 values in total). Print this list to see the evolution of the population.

  3. Encapsulate the logic of your loop into a function called `iterate` that takes the initial population as its first input, the parameter `t_final` as its second input and the parameter `r` as its third input. The function should return the list of values representing the state of the logistic map at times `t = [0,1,...,t_final]`. Run this function for periods `t_final = 100` and `1000` and print some of the values. Is the population trending toward a steady state?

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  1.

  .. code-block:: python
    :linenos:

    def logistic_map(x, r):
        return r * x * (1 - x)

  2.

  .. code-block:: python
    :linenos:

    initial_population = 0.5
    t_final = 10
    r = 1.0
    population = [initial_population]
    for t in range(t_final):
        population.append( logistic_map(population[t], r) )

  3.

  .. code-block:: python
    :linenos:

    def iterate(initial_population, t_final, r):
        population = [initial_population]
        for t in range(t_final):
            population.append( logistic_map(population[t], r) )
        return population

    for period in (10, 100, 1000):
        population = iterate(0.5, period, 1)
        print(population[-1])

  .. code-block::

    0.06945089389714401
    0.009395779870614648
    0.0009913908614406382

  The population seems to be approaching zero.



  .. raw:: html

    </details>

-------------------

.. hint:: **Using Functions With Conditionals in Pandas**

  Functions will often contain conditionals.  Here is a short example that will indicate which quartile the argument is in based on hand-coded values for the quartile cut points.

  .. code-block:: python
    :linenos:

    def calculate_life_quartile(exp):
        if exp < 58.41:
            # This observation is in the first quartile
            return 1
        elif exp >= 58.41 and exp < 67.05:
            # This observation is in the second quartile
          return 2
        elif exp >= 67.05 and exp < 71.70:
            # This observation is in the third quartile
          return 3
        elif exp >= 71.70:
            # This observation is in the fourth quartile
          return 4
        else:
            # This observation has bad data
          return None

    calculate_life_quartile(62.5)

  .. code-block::

    2

  That function would typically be used within a `for` loop, but Pandas has a different, more efficient way of doing the same thing, and that is by *applying* a function to a dataframe or a portion of a dataframe.  Here is an example, using the definition above.

  .. code-block:: python
    :linenos:

    data = pd.read_csv('data/gapminder_all.csv')
    data['life_qrtl'] = data['lifeExp_1952'].apply(calculate_life_quartile)

  There is a lot in that second line, so let's take it piece by piece.

  - On the right side of the `=` we start with `data['lifeExp']`, which is the column in the dataframe called `data` labeled `lifExp`.
  - We use the `apply()` to do what it says, apply the `calculate_life_quartile` to the value of this column for every row in the dataframe.

-------------------

.. admonition:: **Summary**

  - Break programs down into functions to make them easier to understand.
  - Define a function using `def` with a name, parameters, and a block of code.
  - Defining a function does not run it.
  - Arguments in a function call are matched to its defined parameters.
  - Functions may return a result to their caller using `return`.
