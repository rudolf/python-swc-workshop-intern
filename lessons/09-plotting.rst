====================
Lesson 11 - Plotting
====================


Teaching: 15 min
Exercises: 15 min

-------------------

.. highlights:: **Highlights**

  - Create a time series plot showing a single data set.
  - Create a scatter plot showing relationship between two data sets.

.. hint::

  - How can I plot my data?
  - How can I save my plot for publishing?

-------------------

The `matplotlib` library
========================

- `matplotlib <https://matplotlib.org/>`__ is the most widely used scientific plotting library in Python.
- Commonly use a sub-library called `matplotlib.pyplot <https://matplotlib.org/stable/tutorials/introductory/pyplot.html>`__.
- The Jupyter Notebook will render plots inline by default.

.. code-block:: python
  :linenos:

  import matplotlib.pyplot as plt

- Simple plots are then (fairly) simple to create.
- In general there are two accepted methods to work with plots:

  - Using active plots (`plt.plot()`)
  - Using figure and axis objects (`ax.plot()`)

- When creating multiple plots the object oriented method is more transparent because it is clear where things are plot.

.. code-block:: python
  :linenos:

  # Create some data
  time = [0, 1, 2, 3]
  position = [0, 100, 200, 300]

  # Active plots (only uses last active plot)
  plt.plot(time, position)
  plt.xlabel('Time (hr)')
  plt.ylabel('Position (km)')

  # Object oriented version
  fig, ax = plt.subplots()  # Creates a figure and axis object
  ax.plot(time, position)
  ax.set_xlabel('Time (hr)')
  ax.set_ylabel('Position (km)')

.. figure:: fig/9_simple_position_time_plot.svg
  :alt: Simple Position-Time Plot

.. hint:: **Display All Open Figures**

  In our Jupyter Notebook example, running the cell should generate the figure directly below the code. The figure is also included in the Notebook document for future viewing. However, other Python environments like an interactive Python session started from a terminal or a Python script executed via the command line require an additional command to display the figure.

  Instruct `matplotlib` to show a figure:

  .. code-block:: python
    :linenos:

    plt.show()

  This command can also be used within a Notebook - for instance, to display multiple figures if several are created by a single cell.

-------------------

Plotting with `pandas`
----------------------

- Plot data directly from a `Pandas dataframe <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html>`__.
- Before plotting, we convert the column headings from a `string` to `integer` data type, since they represent numerical values, using `str.replace() <https://pandas.pydata.org/docs/reference/api/pandas.Series.str.replace.html>`__ to remove the `gpdPercap_` prefix and then `astype(int) <https://pandas.pydata.org/docs/reference/api/pandas.Series.astype.html>`__ to convert the series of string values (`['1952', '1957', ..., '2007']`) to a series of integers: `[1925, 1957, ..., 2007]`.

.. code-block:: python
  :linenos:

  import pandas as pd

  data = pd.read_csv('data/gapminder_gdp_oceania.csv', index_col='country')

  # Extract year from last 4 characters of each column name
  # The current column names are structured as 'gdpPercap_(year)',
  # so we want to keep the (year) part only for clarity when plotting GDP vs. years
  # To do this we use replace(), which removes from the string the characters stated in the argument
  # This method works on strings, so we use replace() from Pandas Series.str vectorized string functions

  years = data.columns.str.replace('gdpPercap_', '')

  # Convert year values to integers, saving results back to dataframe

  data.columns = years.astype(int)

  data.loc['Australia'].plot()

.. figure:: fig/9_gdp_australia.svg
  :alt: GDP plot for Australia

Select and Transform Data
-------------------------

- By default, `DataFrame.plot <https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.plot.html#pandas.DataFrame.plot>`__ plots with the rows as the X axis.
- We can transpose the data in order to plot multiple series.

.. code-block:: python
  :linenos:

  data.T.plot()
  plt.ylabel('GDP per capita')

.. figure:: fig/9_gdp_australia_nz.svg
  :alt: GDP plot for Australia and New Zealand

Plot Styles
-----------

- Many styles of plot are available.
- For example, do a bar plot using a fancier style.

.. code-block:: python
  :linenos:

  plt.style.use('ggplot')
  data.T.plot(kind='bar')
  plt.ylabel('GDP per capita')

.. figure:: fig/9_gdp_bar.svg
  :alt: GDP barplot for Australia

General Plotting
================

Using the `plot` Function
-------------------------

- Data can also be plotted by calling the `plot` function directly.
- The command is `plot(x, y)`
- The color and format of markers can also be specified as an additional optional argument e.g., `b-` is a blue line, `g--` is a green dashed line.

.. code-block:: python
  :linenos:

  years = data.columns
  gdp_australia = data.loc['Australia']

  fig, ax = plt.subplots()
  ax.plot(years, gdp_australia, 'g--')

.. figure:: fig/9_gdp_australia_formatted.svg
  :alt: GDP formatted plot for Australia


.. hint:: **Useful Additional Options for Plotting**

  All plotting functions usually share common keyword arguments that allow strong customization of each plot. Here are some frequently used arguments:

  **Figure Options**

  .. code-block:: python
    :linenos:

    fig, axes = plot.subplots(
      dpi=150,  # Resolution of the Figure (150=publication, 300=high quality)
      figsize=(10, 4),  # Size of the Figure in inches
      nrows=3,  # Generates 3 rows of axes
      ncols=2  # Generates 2 columns of axes
    )  # -> axes is now an Array of axes (e.g. ax[0][0] is the upper left plot)

  **Plot Options**

  .. code-block:: python
    :linenos:

    plot(
      x, y,  # The data
      fmt='k:',  # A summarized formatting string, black dotted (not compatible with some options shown below)

      alpha=0.5,  # Transparency 0 = Full transparent, 1 = Opaque
      color='w',  # Color all elements of plot (white), there are many possible colors here (see Documentation)
      label='Test Data',  # Label for legend
      linestyle='--',  # Style of the line (- solid, -- dashed, : dotted, etc.)
      linewitdth=2,  # Line Width
      marker='*',  # Marker style: . dot, o circle, * star, h hexagon, etc.
      markeredgecolor='g',  # Color of the marker edge (green)
      markerfacecolor='k',  # Color of the marker face (black)
      markersize=2,  # Size of the marker
      zorder=3,  # Plotting order in the figure (1 = low layer ... 100 upper layer)
    )

  **Axes Options**
  *also for `y`axis*

  .. code-block:: python
    :linenos:

    ax.set_xlabel('Test X-Axis')  # Sets Label of axis
    ax.set_xlim(0, 300)  # Sets axis limits
    ax.set_xticks([0, 30, 40, 100])  # Sets the tick label positions
    ax.set_xticklabels(['First', 'Thirty', 'Forty', 'End'])  # Sets the labels at the ticklabel positions
    ax.xaxis.set_scale('log')  # sets the axis scale (log, lin)


- You can plot many sets of data together.

.. code-block:: python
  :linenos:

  # Select two countries' worth of data.
  gdp_australia = data.loc['Australia']
  gdp_nz = data.loc['New Zealand']

  # Plot with differently-colored markers.
  fig, ax = plt.subplots()
  ax.plot(years, gdp_australia, 'b-', label='Australia')
  ax.plot(years, gdp_nz, 'g-', label='New Zealand')

  # Create legend.
  ax.legend(loc='upper left')
  ax.set_xlabel('Year')
  ax.set_ylabel('GDP per capita ($)')

.. figure:: fig/9_gdp_australia_nz_formatted.svg
  :alt: Formatted plot for Australia and New Zealand

.. hint:: **Adding a Legend**

  Often when plotting multiple datasets on the same figure it is desirable to have a legend describing the data.

  This can be done in `matplotlib` in two stages:

  - Provide a label for each dataset in the figure:

  .. code-block:: python
    :linenos:

    ax.plot(years, gdp_australia, label='Australia')
    ax.plot(years, gdp_nz, label='New Zealand')

  - Instruct `matplotlib` to create the legend.

  .. code-block:: python
    :linenos:

    fig.legend()  # This adds the legend to the figure object (can be outside the axis)
    ax.legend()  # This adds the legend into the axis

  By default matplotlib will attempt to place the legend in a suitable position. If you would rather specify a position this can be done with the `loc=` argument, e.g to place the legend in the upper left corner of the plot, specify `loc='upper left'`.


Saving your plot to a file
--------------------------

If you are satisfied with the plot you see you may want to save it to a file, perhaps to include it in a publication. There is a function in the matplotlib.pyplot module that accomplishes this: `savefig <https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.savefig.html>`__. Calling this function, e.g. with

.. code-block:: python
  :linenos:

  plt.savefig('my_figure.png')

will save the current figure to the file `my_figure.png`. The file format will automatically be deduced from the file name extension (other formats are pdf, ps, eps and svg).

Note that functions in `plt` refer to a global figure variable and after a figure has been displayed to the screen (e.g. with `plt.show`) matplotlib will make this  variable refer to a new empty figure. Therefore, make sure you call `plt.savefig` before the plot is displayed to the screen, otherwise you may find a file with an empty plot.

When using dataframes, data is often generated and plotted to screen in one line. In addition to using `plt.savefig`, we can save a reference to the current figure in a local variable (with `plt.gcf`) and call the `savefig` class method from that variable to save the figure to file.

.. code-block:: python
  :linenos:

  data.plot(kind='bar')
  fig = plt.gcf() # get current figure
  fig.savefig('my_figure.png')


Plot Types
----------

- Commonly used plots are:

  - `scatter(x, y, c, s)`: scatterplot with `c` for colorscale and `s` for size of the markers
  - `bar(x, height)`: barplot, also as horizontal version (`barh()`)
  - `hist(x)`: histogram plot
  - `boxplot(X)`: Boxplot of several data populations (`X` has multiple columns), related: `violinplot()`
  - `errorbar(x, y, yerr, xerr)`: Line plot with errorbars
  - `imshow(Z)` or `matshow(Z)` for gridded 2D data
  - `contour(X,Y,Z)` or `contourf(X,Y,Z)` for gridded 2D data
  - `quiver(X,Y,U,V)` for 2D vector fields


You can find a list of all plot types in the `Matplotlib Documentation <https://matplotlib.org/stable/plot_types/index.html>`__.



.. hint:: **Making your plots accessible**

  Whenever you are generating plots to go into a paper or a presentation, there are a few things you can do to make sure that everyone can understand your plots.

  - Always make sure your text is large enough to read. Use the `fontsize` parameter in `xlabel`, `ylabel`, `title`, and `legend`, and `tick_params` with `labelsize <https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.tick_params.html>`__ to increase the text size of the numbers on your axes.
  - Similarly, you should make your graph elements easy to see. Use `s` to increase the size of your scatterplot markers and `linewidth` to increase the sizes of your plot lines.
  - Using color (and nothing else) to distinguish between different plot elements will make your plots unreadable to anyone who is colorblind, or who happens to have a black-and-white office printer. For lines, the `linestyle` parameter lets you use different types of lines. For scatterplots, `marker` lets you change the shape of your points. If you're unsure about your colors, you can use `Coblis <https://www.color-blindness.com/coblis-color-blindness-simulator/>`__ or `Color Oracle <https://colororacle.org/>`__ to simulate what your plots would look like to those with colorblindness.

-------------------


.. attention:: **Minima and Maxima**

  Fill in the blanks below to plot the minimum GDP per capita over time for all the countries in Europe. Modify it again to plot the maximum GDP per capita over time for Europe.

  .. code-block:: python
    :linenos:

    data_europe = pd.read_csv('data/gapminder_gdp_europe.csv', index_col='country')
    data_europe.____.plot(label='min')
    data_europe.____
    plt.legend(loc='best')
    plt.xticks(rotation=90)

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. code-block:: python
    :linenos:

    data_europe = pd.read_csv('data/gapminder_gdp_europe.csv', index_col='country')
    data_europe.min().plot(label='min')
    data_europe.max().plot(label='max')
    plt.legend(loc='best')
    plt.xticks(rotation=90)

  .. figure:: fig/9_minima_maxima_solution.png
    :alt: Minima Maxima Solution

  .. raw:: html

    </details>

-------------------

.. attention:: **Correlations**

  Modify the example in the notes to create a scatter plot showing the relationship between the minimum and maximum GDP per capita among the countries in Asia for each year in the data set. What relationship do you see (if any)?

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. code-block:: python
    :linenos:

    data_asia = pd.read_csv('data/gapminder_gdp_asia.csv', index_col='country')
    data_asia.describe().T.plot(kind='scatter', x='min', y='max')

  .. figure:: fig/9_correlations_solution1.svg
    :alt: Correlations Solution 1

  No particular correlations can be seen between the minimum and maximum gdp values year on year. It seems the fortunes of asian countries do not rise and fall together.

  .. raw:: html

    </details>

  You might note that the variability in the maximum is much higher than that of the minimum.  Take a look at the maximum and the max indexes:

  .. code-block:: python
    :linenos:

    data_asia = pd.read_csv('data/gapminder_gdp_asia.csv', index_col='country')
    data_asia.max().plot()
    print(data_asia.idxmax())
    print(data_asia.idxmin())

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. figure:: fig/9_correlations_solution2.png
    :alt: Correlations Solution 2

  Seems the variability in this value is due to a sharp drop after 1972. Some geopolitics at play perhaps? Given the dominance of oil producing countries, maybe the Brent crude index would make an interesting comparison? Whilst Myanmar consistently has the lowest gdp, the highest gdb nation has varied more notably.



  .. raw:: html

    </details>

-------------------

.. attention:: **More Correlations**

  This short program creates a plot showing the correlation between GDP and life expectancy for 2007, normalizing marker size by population:

  .. code-block:: python
    :linenos:

    data_all = pd.read_csv('data/gapminder_all.csv', index_col='country')
    data_all.plot(
      kind='scatter',
      x='gdpPercap_2007',
      y='lifeExp_2007',
      s=data_all['pop_2007']/1e6,
    )

  Using online help and other resources, explain what each argument to `plot` does.

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. figure:: fig/9_more_correlations_solution.svg
    :alt: More Correlations Solution

  - A good place to look is the documentation for the plot function - `help(data_all.plot)`.
  - `kind` - As seen already this determines the kind of plot to be drawn.
  - `x` and `y` - A column name or index that determines what data will be placed on the x and y axes of the plot
  - `s` - Details for this can be found in the documentation of plt.scatter. A single number or one value for each data point. Determines the size of the plotted points.

  .. raw:: html

    </details>

-------------------

.. admonition:: **Summary**

  - `matplotlib <https://matplotlib.org/>`__ is the most widely used scientific plotting library in Python.
  - Plot data directly from a Pandas dataframe.
  - Select and transform data, then plot it.
  - Many styles of plot are available: see the `Python Graph Gallery <https://python-graph-gallery.com/matplotlib/>`__ for more options.
  - Can plot many sets of data together.
