=============================================
Lesson 12 - Reading and Writing Complex Files
=============================================

Teaching: 45 min
Exercises: 15 min

-------------------

.. highlights:: **Highlights**

  - Understand how data is actually read into the computer.
  - Be able to decide which file type is used for which data.

.. hint::

  - How can I read the output of my measurements/simulations?
  - How can I share data with other people?

-------------------


Tabular Data in `weird` Formats
-------------------------------

- Sometimes, especially on German computers, tabular data might not be formatted according to what `pandas` or other modules expect to find.
- Common problems are:

  - Delimiter is `;` instead of `,`
  - Numerical format of floats is `1,0` instead of `1.0`
  - Mixed data in columns, e.g. text or dates
  - Non-tabular data

- Depending on the type of data you need to use special functions or modules to read the data
- The most basic way to read any kind of data from a file is to use the `open()` function:
- Suppose you have a text file named `weather_data.dat` which contains the following:

.. code-block:: text

  #Datei erstellt am: 05.05.2023 13:25
  #Stationsname: Fürth-Krumbach
  #Gewässer:
  #Stationsnummer: 2394101
  #Betreiber: RPU Darmstadt
  #X-Koordinate [ETRS89/ UTM Zone 32N]: 485937.39
  #Y-Koordinate [ETRS89/ UTM Zone 32N]: 5501522.33
  #Parameter: Niederschlag
  #Zeitreihenname: S.P.TSum.0730
  #Zeitliche Auflösung: Tagessummen
  #Einheit: mm
  #Von: 01.01.1976 06:30
  #Bis: 04.05.2023 07:30
  #Zeitbezug: MEZ
  #Datenquelle: HLNUG
  Zeitpunkt	Messwert	Status
  01.01.1976 06:30	2	geprüft
  02.01.1976 06:30	10,2	geprüft
  03.01.1976 06:30	1,1	geprüft
  04.01.1976 06:30	0	geprüft
  05.01.1976 06:30	4,9	geprüft
  06.01.1976 06:30	0,6	geprüft
  07.01.1976 06:30	0,1	geprüft
  08.01.1976 06:30	0	geprüft
  09.01.1976 06:30	0	geprüft
  10.01.1976 06:30	12,9	geprüft
  11.01.1976 06:30	0	geprüft
  12.01.1976 06:30	0	geprüft
  13.01.1976 06:30	1	geprüft
  14.01.1976 06:30	4,3	geprüft
  15.01.1976 06:30	5,6	geprüft
  16.01.1976 06:30	0	geprüft
  17.01.1976 06:30	0	geprüft
  .
  .
  .

- You can notice several things:

  - There is a header containing important metadata.
  - The data has column headers that we might want to use as variable names.
  - The data uses tabs as column and `,` as decimal separator.
  - The data has a dateformat, float and text format.

- Suppose you have several of these files and want to read all of them into your Python Script and visualize the contents.

.. attention:: **Storing Data**

  Look at the above file and think about what kind of variable you want to use for each data point.

  How would you store all data together in a single data structure so that it is easy to understand and use with Python?

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  - Dates as `datetime`.
  - Text as `str`.
  - Stationsnummer as `int`.
  - Coordinates as `float` or `Tuple (float, float)`, or for later purposes as `shapely.Point`.
  - The time series as `list` or `numpy.ndarrays` of the respective data type.

  - The whole dataset including additional information could be stored in a `dict`.

  .. raw:: html

    </details>

.. attention:: **Identifying Elements to Read Data**

  If you go through the file, line by line, how would you be able to identify what kind of data format should be used?

  How would you separate values from keywords?

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  - Metadata conveniently has a `#` sign to identify it, so all lines starting with `#` should be identified as metadata.
  - The actual data and column headers do not have a `#` sign
  - For Metadata the values are separated from the keywords by `:` while for the actual data it is a tab.

  .. raw:: html

    </details>

Using `open()` to read text data
--------------------------------

- Exactly as you did just now in the exercise we will tell Python to go through the file line by line and decide where and how to store the data.
- First we need to open the file in order to be able to read from it, just like in a text editor.
- Then we can look at what Python was able to read without any further information:

.. code-block:: python
  :linenos:

  with open("weather_data.dat", "rt") as dat_file:
      read_data = dat_file.read()

  print(read_data)

.. code-block:: output

  #Datei erstellt am: 05.05.2023 13:25
  #Stationsname: FÃ¼rth-Krumbach
  #GewÃ¤sser:
  #Stationsnummer: 2394101
  #Betreiber: RPU Darmstadt
  #X-Koordinate [ETRS89/ UTM Zone 32N]: 485937.39
  #Y-Koordinate [ETRS89/ UTM Zone 32N]: 5501522.33
  #Parameter: Niederschlag
  #Zeitreihenname: S.P.TSum.0730
  #Zeitliche AuflÃ¶sung: Tagessummen
  #Einheit: mm
  #Von: 01.01.1976 06:30
  #Bis: 04.05.2023 07:30
  #Zeitbezug: MEZ
  #Datenquelle: HLNUG
  Zeitpunkt       Messwert        Status
  01.01.1976 06:30        2       geprÃ¼ft
  02.01.1976 06:30        10,2    geprÃ¼ft
  03.01.1976 06:30        1,1     geprÃ¼ft
  04.01.1976 06:30        0       geprÃ¼ft
  05.01.1976 06:30        4,9     geprÃ¼ft
  06.01.1976 06:30        0,6     geprÃ¼ft
  07.01.1976 06:30        0,1     geprÃ¼ft
  08.01.1976 06:30        0       geprÃ¼ft
  09.01.1976 06:30        0       geprÃ¼ft
  10.01.1976 06:30        12,9    geprÃ¼ft
  11.01.1976 06:30        0       geprÃ¼ft
  12.01.1976 06:30        0       geprÃ¼ft
  13.01.1976 06:30        1       geprÃ¼ft
  14.01.1976 06:30        4,3     geprÃ¼ft
  15.01.1976 06:30        5,6     geprÃ¼ft
  16.01.1976 06:30        0       geprÃ¼ft
  17.01.1976 06:30        0       geprÃ¼ft

- Using `with open() as ...:` is good practice here because this automatically closes the file when leaving the indented code environment. This helps to free resources and avoids locking the files or accidentally opening it several times.
- The argument `rt` tells the function to open the file in **r** ead mode for **t** ext. There are other specifiers such as **w** rite, **a** ppend or **r+** ead/write, and **b** inary format.
- As we can see there are several issues here:

  - Umlauts have a weird format. This is because `open()` uses the encoding of text into binary (how the computer stores data) from your system. On Windows this is ANSI while for most other systems it is `utf-8`.
  - We have read all data as a block and not separated into data.

- Actually it is even worse: During print some information in the files actually have been formatted for us, such as the carriage returns and tabulators. The `actual` content of the file how it looks to your computer is this (seen by using `print(repr(read_data))`):

.. code-block:: output

  '#Datei erstellt am: 05.05.2023 13:25\n#Stationsname: FÃ¼rth-Krumbach\n#GewÃ¤sser:\n#Stationsnummer: 2394101\n#Betreiber: RPU Darmstadt\n#X-Koordinate [ETRS89/ UTM Zone 32N]: 485937.39\n#Y-Koordinate [ETRS89/ UTM Zone 32N]: 5501522.33\n#Parameter: Niederschlag\n#Zeitreihenname: S.P.TSum.0730\n#Zeitliche AuflÃ¶sung: Tagessummen\n#Einheit: mm\n#Von: 01.01.1976 06:30\n#Bis: 04.05.2023 07:30\n#Zeitbezug: MEZ\n#Datenquelle: HLNUG\nZeitpunkt\tMesswert\tStatus\n01.01.1976 06:30\t2\tgeprÃ¼ft\n02.01.1976 06:30\t10,2\tgeprÃ¼ft\n03.01.1976 06:30\t1,1\tgeprÃ¼ft\n04.01.1976 06:30\t0\tgeprÃ¼ft\n05.01.1976 06:30\t4,9\tgeprÃ¼ft\n06.01.1976 06:30\t0,6\tgeprÃ¼ft\n07.01.1976 06:30\t0,1\tgeprÃ¼ft\n08.01.1976 06:30\t0\tgeprÃ¼ft\n09.01.1976 06:30\t0\tgeprÃ¼ft\n10.01.1976 06:30\t12,9\tgeprÃ¼ft\n11.01.1976 06:30\t0\tgeprÃ¼ft\n12.01.1976 06:30\t0\tgeprÃ¼ft\n13.01.1976 06:30\t1\tgeprÃ¼ft\n14.01.1976 06:30\t4,3\tgeprÃ¼ft\n15.01.1976 06:30\t5,6\tgeprÃ¼ft\n16.01.1976 06:30\t0\tgeprÃ¼ft\n17.01.1976 06:30\t0\tgeprÃ¼ft\n'

- You can see that there are no new lines and all data is just in a straight line.
- Besides the normal characters, computers rely on special characters to store newlines `\\n` or tabs `\\t`. On some systems a newline character is preceded by a carriage return `\\r` which stems from the ancient past where typewriters and printers either had a paper carriage or printing head that had to be moved back.
- To address our problems we can do the following changes to our code:

.. code-block:: python
  :linenos:

  with open("weather_data.dat", "rt", encoding="utf-8") as dat_file:
      for line in dat_file:
          print(line, end="")

- The keyword argument `encoding` now ensures that the file content is interpreted correctly.
- The file object `dat_file` is iterable which reads a single line each time it is called, hence we can loop over it by using a for loop.
- Because each line already contains `\\n` at the end we can tell `print()` to end each line by an empty string.

Reading the Data
----------------

Now that we can loop over the content of each file we can use the tools we already know to dissect the lines into values:

.. code-block:: python
  :linenos:

  with open("weather_data.dat", "rt", encoding="utf-8") as dat_file:
      # flag to see if we already reached the data
      real_data = False
      for line in dat_file:
          # See if we have normal or Metadata:
          if line.startswith("#"):
              separated = line[1:-1].split(": ")
              print(separated)

          # If not we arrived at the "real" data:
          else:
              # The first time we are still at the headers:
              if not real_data:
                  separated_header = line[:-1].split("\t")
                  print(separated_header)
                  # after we have read this we can set our flag to True
                  real_data = True
              else:
                  # Now we can read the rest as data:
                  separated_data = line[:-1].split("\t")
                  print(separated_data)


.. code-block:: output

  ['Datei erstellt am', '05.05.2023 13:25']
  ['Stationsname', 'Fürth-Krumbach']
  ['Gewässer', 'Krumbach']
  ['Stationsnummer', '2394101']
  ['Betreiber', 'RPU Darmstadt']
  ['X-Koordinate [ETRS89/ UTM Zone 32N]', '485937.39']
  ['Y-Koordinate [ETRS89/ UTM Zone 32N]', '5501522.33']
  ['Parameter', 'Niederschlag']
  ['Zeitreihenname', 'S.P.TSum.0730']
  ['Zeitliche Auflösung', 'Tagessummen']
  ['Einheit', 'mm']
  ['Von', '01.01.1976 06:30']
  ['Bis', '04.05.2023 07:30']
  ['Zeitbezug', 'MEZ']
  ['Datenquelle', 'HLNUG']
  ['Zeitpunkt', 'Messwert', 'Status']
  ['01.01.1976 06:30', '2', 'geprüft']
  ['02.01.1976 06:30', '10,2', 'geprüft']
  ['03.01.1976 06:30', '1,1', 'geprüft']
  ['04.01.1976 06:30', '0', 'geprüft']
  ['05.01.1976 06:30', '4,9', 'geprüft']
  ['06.01.1976 06:30', '0,6', 'geprüft']
  ['07.01.1976 06:30', '0,1', 'geprüft']
  ['08.01.1976 06:30', '0', 'geprüft']
  ['09.01.1976 06:30', '0', 'geprüft']
  ['10.01.1976 06:30', '12,9', 'geprüft']
  ['11.01.1976 06:30', '0', 'geprüft']
  ['12.01.1976 06:30', '0', 'geprüft']
  ['13.01.1976 06:30', '1', 'geprüft']
  ['14.01.1976 06:30', '4,3', 'geprüft']
  ['15.01.1976 06:30', '5,6', 'geprüft']
  ['16.01.1976 06:30', '0', 'geprüft']
  ['17.01.1976 06:30', '0', 'geprüft']

- You can see that each line is now separated into lists of strings containing the data we want.
- Right now the data is only in strings, not in float nor date format. We cannot use them for calculating. To convert everything into a useable format we need to convert the strings into values.
- We also use the metadata and column headers to create a dictionary for easier data access.

.. code-block:: python
  :linenos:

  import datetime

  with open(
      "weather_data.dat",
      "rt",
      encoding="utf-8",
  ) as dat_file:
      # flag to see if we already reached the data
      real_data = False

      # Precreate a dictionary to be filled with data:
      data = dict()

      for line in dat_file:
          # See if we have normal or Metadata:
          if line.startswith("#"):
              separated = line[1:-1].split(": ")
              key = separated[0]
              value = separated[1]

              # To distinguish the type of data we look for contents in the keys:
              if "Datei" in key or "Von" in key or "Bis" in key:
                  value_converted = datetime.datetime.strptime(
                      value, "%d.%m.%Y %H:%M"
                  )
              elif "Stationsnummer" in key:  # Positive number
                  value_converted = int(value)
              elif "Koordinate" in key:
                  value_converted = float(value)
              else:
                  value_converted = value

              # After conversion we add the data to the dictionary:
              data[key] = value_converted

          # If not we arrived at the "real" data:
          else:
              # The first time we are still at the headers:
              if not real_data:
                  separated_header = line[:-1].split("\t")

                  # The headers define the names for the data lists:
                  for header in separated_header:
                      data[header] = []  # Preallocate an empty list

                  # after we have read this we can set our flag to True
                  real_data = True
              else:
                  # Now we can read the rest as data:
                  separated_data = line[:-1].split("\t")

                  # Append each datapoint to its list
                  data["Zeitpunkt"].append(
                      datetime.datetime.strptime(
                          separated_data[0], "%d.%m.%Y %H:%M"
                      )
                  )
                  data["Messwert"].append(
                      float(separated_data[1].replace(",", "."))
                  )
                  data["Status"].append(separated_data[2])

  # Show the contents of data
  for key in data.keys():
      print(key+':', data[key])

.. code-block:: output

  Stationsname: Fürth-Krumbach
  Gewässer: Krumbach
  Stationsnummer: 2394101
  Betreiber: RPU Darmstadt
  X-Koordinate [ETRS89/ UTM Zone 32N]: 485937.39
  Y-Koordinate [ETRS89/ UTM Zone 32N]: 5501522.33
  Parameter: Niederschlag
  Zeitreihenname: S.P.TSum.0730
  Zeitliche Auflösung: Tagessummen
  Einheit: mm
  Von: 1976-01-01 06:30:00
  Bis: 2023-05-04 07:30:00
  Zeitbezug: MEZ
  Datenquelle: HLNUG
  Zeitpunkt: [datetime.datetime(1976, 1, 1, 6, 30), datetime.datetime(1976, 1, 2, 6, 30), datetime.datetime(1976, 1, 3, 6, 30), datetime.datetime(1976, 1, 4, 6, 30), datetime.datetime(1976, 1, 5, 6, 30), datetime.datetime(1976, 1, 6, 6, 30), datetime.datetime(1976, 1, 7, 6, 30), datetime.datetime(1976, 1, 8, 6, 30), datetime.datetime(1976, 1, 9, 6, 30), datetime.datetime(1976, 1, 10, 6, 30), datetime.datetime(1976, 1, 11, 6, 30), datetime.datetime(1976, 1, 12, 6, 30), datetime.datetime(1976, 1, 13, 6, 30), datetime.datetime(1976, 1, 14, 6, 30), datetime.datetime(1976, 1, 15, 6, 30), datetime.datetime(1976, 1, 16, 6, 30), datetime.datetime(1976, 1, 17, 6, 30)]
  Messwert: [2.0, 10.2, 1.1, 0.0, 4.9, 0.6, 0.1, 0.0, 0.0, 12.9, 0.0, 0.0, 1.0, 4.3, 5.6, 0.0, 0.0]
  Status: ['geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft', 'geprüft']

.. attention:: **Alternative Data Conversion**

  Can you modify this segment in the above code to avoid using the keys directly (`"Zeitpunkt"`, `"Messwert"` and `"Status"`)?

  .. code-block:: python
    :linenos:

    separated_data = line[:-1].split("\t")

    # Append each datapoint to its list
    data["Zeitpunkt"].append(
        datetime.datetime.strptime(
            separated_data[0], "%d.%m.%Y %H:%M"
        )
    )
    data["Messwert"].append(
        float(separated_data[1].replace(",", "."))
    )
    data["Status"].append(separated_data[2])

  .. hint::

    Try using `enumerate` and a `for` loop.

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. code-block:: python
    :linenos:

    separated_data = line[:-1].split("\t")
    # First convert the data
    converted_data = [
        datetime.datetime.strptime(
            separated_data[0], "%d.%m.%Y %H:%M"
        ),
        float(separated_data[1].replace(",", ".")),
        separated_data[2],
    ]
    # Then append each datapoint to its list
    for ii, key in enumerate(separated_header):
        data[key].append(converted_data[ii])

  This is a more flexible way, in case the naming scheme changes for the files. However we still need to manually convert the data first.

  .. raw:: html

    </details>

.. hint::

  This is a very basic way of reading files to help you understand how it works under the hood. However, in many cases it is better to already think about your data analysis when saving the data.

  For example in Excel you can already modify to save CSV files in the right format. Some programs already save the data in Python compatible files.

  The `csv` module allows for a much quicker csv file reading, including the definition of separators etc... More in the `Python Documentation for csv <https://docs.python.org/3/library/csv>`__.

Plotting the Data
-----------------

After you have read the data you can plot it with matplotlib without any further modifications:

.. code-block:: python

  import matplotlib.pyplot as plt

  fig, ax = plt.subplots()
  ax.bar(data["Zeitpunkt"], data["Messwert"])
  ax.set_xlabel("Zeit")
  ax.set_ylabel(f'{data["Parameter"]} ({data["Einheit"]})')
  ax.set_title(f'{data["Parameter"]} in {data["Stationsname"]}')
  plt.show()

- Here we also make use of `f-Strings` to incorporate the data read from the file.

Storing Data
------------

In a similar manner you can store data using:

.. code-block:: python
  :linenos:

  with open("output.dat", "wt", encoding="utf-8") as output_file:
      output_file.write("Zeitpunkt, Messwert\n")
      for tt,mm in zip(data["Zeitpunkt"], data["Messwert"]):
          output_file.write(f"{tt},{mm}\n")

.. code-block:: text

  Zeitpunkt, Messwert
  1976-01-01 06:30:00,2.0
  1976-01-02 06:30:00,10.2
  1976-01-03 06:30:00,1.1
  1976-01-04 06:30:00,0.0
  1976-01-05 06:30:00,4.9
  1976-01-06 06:30:00,0.6
  1976-01-07 06:30:00,0.1
  1976-01-08 06:30:00,0.0
  1976-01-09 06:30:00,0.0
  1976-01-10 06:30:00,12.9
  1976-01-11 06:30:00,0.0
  1976-01-12 06:30:00,0.0
  1976-01-13 06:30:00,1.0
  1976-01-14 06:30:00,4.3
  1976-01-15 06:30:00,5.6
  1976-01-16 06:30:00,0.0
  1976-01-17 06:30:00,0.0


- This first writes a header and then loops over all data and stores it into the file.
- However, we also use our dictionary structure... There is a Python Module to store the dictionary without any major reformatting: `json`.
- This can encode almost all datatypes but because we here use datetimes we also need `django` to get the right encoder.

.. code-block:: python
  :linenos:

  import json
  from django.core.serializers.json import DjangoJSONEncoder

  with open("output.json","wt",encoding="utf-8") as json_file:
      json.dump(data, json_file, cls=DjangoJSONEncoder)

.. code-block:: output

  {
      "Betreiber": "RPU Darmstadt",
      "Bis": "2023-05-04T07:30:00",
      "Datei erstellt am": "2023-05-05T13:25:00",
      "Datenquelle": "HLNUG",
      "Einheit": "mm",
      "Gew\u00e4sser": "Krumbach",
      "Messwert": [
          2.0,
          10.2,
          1.1,
          0.0,
          4.9,
          0.6,
          0.1,
          0.0,
          0.0,
          12.9,
          0.0,
          0.0,
          1.0,
          4.3,
          5.6,
          0.0,
          0.0
      ],
      "Parameter": "Niederschlag",
      "Stationsname": "F\u00fcrth-Krumbach",
      "Stationsnummer": 2394101,
      "Status": [
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft",
          "gepr\u00fcft"
      ],
      "Von": "1976-01-01T06:30:00",
      "X-Koordinate [ETRS89/ UTM Zone 32N]": 485937.39,
      "Y-Koordinate [ETRS89/ UTM Zone 32N]": 5501522.33,
      "Zeitbezug": "MEZ",
      "Zeitliche Aufl\u00f6sung": "Tagessummen",
      "Zeitpunkt": [
          "1976-01-01T06:30:00",
          "1976-01-02T06:30:00",
          "1976-01-03T06:30:00",
          "1976-01-04T06:30:00",
          "1976-01-05T06:30:00",
          "1976-01-06T06:30:00",
          "1976-01-07T06:30:00",
          "1976-01-08T06:30:00",
          "1976-01-09T06:30:00",
          "1976-01-10T06:30:00",
          "1976-01-11T06:30:00",
          "1976-01-12T06:30:00",
          "1976-01-13T06:30:00",
          "1976-01-14T06:30:00",
          "1976-01-15T06:30:00",
          "1976-01-16T06:30:00",
          "1976-01-17T06:30:00"
      ],
      "Zeitreihenname": "S.P.TSum.0730"
  }

- You can add the keywords `sort_keys=True, indent=4` for prettier file formatting.

.. hint::
  Note how the encoding handles datetime and Umlauts!

- To read the data back in:

.. code-block:: python
  :linenos:

  with open("output.json","wt",encoding="utf-8") as json_file:
    data_loaded = json.load(json_file)

.. attention::

  Be aware that for this example the datetimes are now read as strings. You'll have to convert them back to datetime!

Pickling Data
-------------

Another method of storing data, especially larger arrays with many numbers is `pickle`.

- It works natively with all Python data types, however it is not human-readable and Python-specific.
- JSON is more common but reaches its limits rather quickly with only a few 1000s of values.

.. code-block:: python
  :linenos:

  import pickle

  with open("output.pkl", "wb") as pkl_file:
      pickle.dump(data, pkl_file)


  with open("output.pkl", "rb") as pkl_file:
      pickle_loaded = pickle.load(pkl_file)
      print(pickle_loaded)

- The syntax is almost exactly the same as for the other methods, however you have to open the file in binary mode with `b`.

.. attention:: **File Sizes**

  Take a look at the size the different files use on your harddrive. This example is not exactly the most representative, but for very large timeseries the pickle should be much smaller.

  Do you know why?

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  In text encoded files, all numbers are stored as strings. For large numbers or floats with many significant digits this takes up more bytes per number than in binary format.

  A single character uses 8 bit (hence `utf-8`), while a float uses 32 or 64 bit. Therefore, if your numbers have 4 (or 8) or more significant digits, they will take up more space as text than as binary.

  Additionally, there is no big conversion necessary as for pickles, the data is already stored similar to how Python needs it.

  .. raw:: html

    </details>


.. admonition:: **Summary**

  - Use `open()` to iteratively read a tabular file.
  - Use `json` to store dictionary-style data.
  - Use `pickle` to store large amounts of numeric data.