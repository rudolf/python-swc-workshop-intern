==========================
Lesson 14 - Variable Scope
==========================

Teaching: 10 min
Exercises: 10 min

-------------------

.. highlights:: **Highlights**

  - Identify local and global variables.
  - Identify parameters as local variables.
  - Read a traceback and determine the file, function, and line number on which the error occurred, the type of error, and the error message.

.. hint::

  - How do function calls actually work?
  - How can I determine where errors occurred?

-------------------

Variable Scope
==============

- The scope of a variable is the part of a program that can 'see' that variable.
- There are only so many sensible names for variables.
- People using functions shouldn't have to worry about what variable names the author of the function used.
- People writing functions shouldn't have to worry about what variable names the function's caller uses.
- The part of a program in which a variable is visible is called its *scope*.

.. code-block:: python
  :linenos:

  pressure = 103.9

  def adjust(t):
      temperature = t * 1.43 / pressure
      return temperature

- `pressure` is a *global variable*.

  - Defined outside any particular function.
  - Visible everywhere.

- `t` and `temperature` are *local variables* in `adjust`.

  - Defined in the function.
  - Not visible in the main program.
  - Remember: a function parameter is a variable that is automatically assigned a value when the function is called.

.. code-block:: python
  :linenos:

  print('adjusted:', adjust(0.9))
  print('temperature after call:', temperature)

.. code-block::

  adjusted: 0.01238691049085659
  Traceback (most recent call last):
    File "/Users/swcarpentry/foo.py", line 8, in <module>
      print('temperature after call:', temperature)
  NameError: name 'temperature' is not defined

We get a `NameError` because the variable temperature does not exist outside the function definition. It only exists in the scope of the function. A nested sequence of function calls, each with their respective variable scope is called `call stack`.

.. hint::

  Most of the time errors occur in functions of libraries. Python is able to trace back where the last error occurred. However, this usually does not mean that there is a problem in the library, but rather a problem with your function inputs. So you should move up the call stack and slowly trace backwards where things go wrong.

.. attention:: **Local and Global Variable Use**

  Trace the values of all variables in this program as it is executed.
  (Use '---' as the value of variables before and after they exist.)

  .. code-block:: python
    :linenos:

    limit = 100
    def clip(value):
        return min(max(0.0, value), limit)
    measurement = -22.5
    print(clip(measurement))
    print("Done!")

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  .. code-block::

    1: limit = 100, value = -----, measurement = -----
    2: limit = 100, value = -----, measurement = -----
    3: limit = 100, value = -----, measurement = -----
    4: limit = 100, value = -----, measurement = -22.5
    5: limit = 100, value = -22.5, measurement = -22.5
    6: limit = 100, value = -----, measurement = -22.5


  .. raw:: html

    </details>

-------------------

.. attention:: **Reading Error Messages**

  Read the traceback below, and identify the following:

  1. How many levels does the traceback have?
  2. What is the file name where the error occurred?
  3. What is the function name where the error occurred?
  4. On which line number in this function did the error occur?
  5. What is the type of error?
  6. What is the error message?

  .. code-block::

    ---------------------------------------------------------------------------
    KeyError                                  Traceback (most recent call last)
    <ipython-input-2-e4c4cbafeeb5> in <module>()
          1 import errors_02
    ----> 2 errors_02.print_friday_message()

    /Users/ghopper/thesis/code/errors_02.py in print_friday_message()
        13
        14 def print_friday_message():
    ---> 15     print_message("Friday")

    /Users/ghopper/thesis/code/errors_02.py in print_message(day)
          9         "sunday": "Aw, the weekend is almost over."
        10     }
    ---> 11     print(messages[day])
        12
        13

    KeyError: 'Friday'

  .. raw:: html

    <details>
      <summary markdown="span"><b>Solution</b></summary>

  1. Three levels.
  2. `errors_02.py`
  3. `print_message`
  4. Line 11
  5. `KeyError`. These errors occur when we are trying to look up a key that does not exist (usually in a data structure such as a dictionary). We can find more information about the `KeyError` and other built-in exceptions in the `Python docs <https://docs.python.org/3/library/exceptions.html#KeyError>`__.
  6. `KeyError: 'Friday'`

  .. raw:: html

    </details>

-------------------

.. admonition:: **Summary**

  - The scope of a variable is the part of a program that can 'see' that variable.
