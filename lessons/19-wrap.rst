=============
Wrap-Up Day 2
=============


Teaching: 10 min
Exercises: 0 min

-------------------

.. highlights:: **Highlights**

  - Name and locate scientific Python community sites for software, workshops, and help.

.. hint::

  - What have we learned?
  - What else is out there and where do I find it?

-------------------

Leslie Lamport once said, "Writing is nature's way of showing you how sloppy your thinking is." The same is true of programming: many things that seem obvious when we're thinking about them turn out to be anything but when we have to explain them precisely.


Some Helpful Links
==================

- Python supports a large and diverse community across academia and industry.

- The `Python 3 documentation <https://docs.python.org/3/>`__ covers the core language and the standard library.
- `PyCon <https://pycon.org/>`__ is the largest annual conference for the Python community.
- `Jupyter <https://jupyter.org>`__ is the home of Project Jupyter.
- Stack Overflow's `general Python section <https://stackoverflow.com/questions/tagged/python?tab=Votes>`__ can be helpful,

  - as well as the sections on `NumPy <https://stackoverflow.com/questions/tagged/numpy?tab=Votes>`__,
  - `SciPy <https://stackoverflow.com/questions/tagged/scipy?tab=Votes>`__, and
  - `Pandas <https://stackoverflow.com/questions/tagged/pandas?tab=Votes>`__.

.. hint:: **Useful Libraries**

  **Internal (comes with Python)**

  - `datetime <https://docs.python.org/3/library/datetime>`__: for working with times and dates (also understood by `matplotlib`, `numpy`)
  - `csv <https://docs.python.org/3/library/csv>`__: Basic reader for csv files
  - `multiprocessing <https://docs.python.org/3/library/multiprocessing>`__: Utilities for parallel processing and multithreading
  - `os <https://docs.python.org/3/library/os>`__: Operating system interface (e.g. working with file paths through `os.path`)
  - `shutil <https://docs.python.org/3/library/shutil>`__: Highlevel file operations (copy, move)
  - `sqlite3 <https://docs.python.org/3/library/sqlite3>`__: Interface for SQLite databases (e.g. `gpkg` files)
  - `logging <https://docs.python.org/3/library/logging>`__: Logging utility for debugging
  - `tkinter <https://docs.python.org/3/library/tkinter>`__: User interfaces, also external e.g. `pyqt <https://www.riverbankcomputing.com/software/pyqt/>`__

  **External**

  - `NumPy <https://numpy.org>`__: Standard library for working with matrices and data in general
  - `SciPy <https://scipy.org>`__: Rich library for scientific computing, curve fitting, statistics, matrix algebra, signal processing...
  - `Pandas <https://pandas.pydata.org>`__: Library for tabular data and general data analysis
  - `Matplotlib <https://matplotlib.org>`__: Defacto standard plotting library
  - `SciKit Learn <https://scikit-learn.org>`__: Machine Learning in Python
  - `SciKit Image <https://scikit-image.org>`__: Image Processing in Python
  - `GeoPandas <https://geopandas.org>`__: Library to work with GIS data, based on pandas
  - Other GIS libraries: `shapely <https://shapely.readthedocs.io/>`__ (Shapefiles), `rasterio <https://rasterio.readthedocs.io/>`__ (Raster Data), `osmnx <https://osmnx.readthedocs.io/en/stable/>`__ (Open-Street-Map API access)
  - Utilities: `tqdm <https://tqdm.github.io/>`__ (progressbar for loops), `numba <https://numba.pydata.org/>`__ (parallel algebraic operations)
