============================================
Programming and Plotting and Python Workshop
============================================

Lessons Overview
================

.. toctree::
   :maxdepth: 1
   :caption: Part 1 - Programming in Python:

   lessons/01-run-quit.rst
   lessons/02-variables.rst
   lessons/03-types-conversion.rst
   lessons/04-built-in.rst
   lessons/05-coffee.rst
   lessons/11-lists.rst
   lessons/12-for-loops.rst
   lessons/13-conditionals.rst
   lessons/06-libraries.rst
   lessons/10-lunch.rst

.. toctree::
   :maxdepth: 1
   :caption: Part 2 - Plotting in Python

   lessons/07-reading-tabular.rst
   lessons/08-data-frames.rst
   lessons/09-plotting.rst
   lessons/14-custom-lesson.rst
   lessons/15-coffee.rst
   lessons/16-writing-functions.rst
   lessons/17-scope.rst
   lessons/18-style.rst
   lessons/19-wrap.rst

References
==========

The workshop materials are based on the `Python Novice Gapminder lesson by the Carpentries <https://github.com/swcarpentry/python-novice-gapminder>`__ with some custom modifications.

- `software-carpentry.org <https://software-carpentry.org>`__
